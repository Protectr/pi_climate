import thunk from 'redux-thunk';
import logger from 'redux-logger';
import {basicReducer} from "./reducer";
import { applyMiddleware, createStore } from 'redux';


export const store = createStore(
    basicReducer,
    {
        settings: {
            temperature: '',
            humidity: '',
            exhaustVent: false,
            light: false,
        },
        sensors_data: {
            temperature: '',
            humidity: ''
        },
        fetched: false,
        is_logged: false,
        error: null,
        user: {
            name: '',
            token: ''
        }
    },
    applyMiddleware(thunk, logger)
);