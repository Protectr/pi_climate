import * as actionTypes from "../actions/types";

/**
 * @name RootState
 * @property {Object} settings
 * @property {boolean} fetched
 * @property {Object} env_params
 * @property {boolean} is_logged
 * @property {any} error
 * @property {Object} user
 */

/**
 * @name user
 * @property {string} name
 * @property {}
 */

/**
 * @name Setting
 * @property {number} id
 * @property {string} title
 * @property {string} poster_path
 */

/**
 *
 * @type RootState
 */
const initialState = {
    settings: {
        temperature: '',
        humidity: '',
        exhaustVent: false,
        light: false,
    },
    sensor_data: {
        temperature: '',
        humidity: '',
    },
    fetched: false,
    is_logged: false,
    error: null,
    user: {
        name: '',
        token: ''
    }
};

export const basicReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_STARTED:
            return {...state, fetched: false };
        case actionTypes.FETCH_SUCCESS:
            return { ...state, fetched: true, settings: action.payload };
        case actionTypes.FETCH_ERROR:
            return { ...state, fetched: true, error: action.payload };
        case actionTypes.LOGIN_ERROR:
            return {...state, is_logged: false };
        case actionTypes.SET_SENSOR_DATA:
            return {...state, sensors_data: {temperature: action.payload.sensors.temperature, humidity: action.payload.sensors.humidity } };
        case actionTypes.SET_SETTINGS_DATA:
            const setting_data = {};
            const key = action.payload.key;
            setting_data[key] = action.payload.val;
            return {...state, settings: {...state.settings, setting_data} };
        case actionTypes.LOGIN_SUCCESS:
            return {...state, is_logged: true, user: {name: action.payload.user.name, token: action.payload.token}};
        default:
            return state
    }
}