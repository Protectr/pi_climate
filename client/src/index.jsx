import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app'
import ErrorBoundary from './components/errorboundary';
import { BrowserRouter } from 'react-router-dom';
import * as actions from "./actions/actions";
import { store } from './reducers/store';
import { Provider } from "react-redux";

store.dispatch(actions.initialFetch());

let components = (
    <BrowserRouter>
        <Provider store={store}>
            <ErrorBoundary >
                <App />
            </ErrorBoundary>
        </Provider>
    </BrowserRouter>
);


ReactDOM.render(components, document.getElementById('app'));

// for hot module replacement
module.hot.accept(); 
