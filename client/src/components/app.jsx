import React, { Component } from 'react';
import './style/style.scss';
import Header from './header';
import { Route, Switch } from 'react-router-dom'
import Settings from './settings';
import Login from "./login";


export default class App extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        if(!this.props) return null;
        return (
            <div>
                <Header/>
                <Switch>
                    <Route path="/" exact={true} component={Login}  />
                    <Route path="/settings" component={Settings} />
                </Switch>
            </div>
        );
    }
}


