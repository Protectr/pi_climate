import React from 'react';
import {store} from "../../reducers/store";
import {FormControl} from 'react-bootstrap';
import styled from 'styled-components';
import FaUp from 'react-icons/lib/fa/angle-double-up';
import FaDown from 'react-icons/lib/fa/angle-double-down';
import cookie from "react-cookies";
import setAuthorizationToken from "../../utils/setAuthorizationToken";
import axios from "axios/index";
import * as actions from "../../actions/actions";

const Clickable = styled.span`
    font-size: 1.5em;
    cursor: pointer;
`;

const inputStyles = {
    width: '150px',
    margin: '10px 0',
    fontSize: '18px',
    textAlign: 'center',
    background: 'transparent',
    color: 'aqua',
    borderRadius: '0',
    borderColor: 'aqua'
};

const wrapstyle1 = {
    width: '80%',
    margin: '10px auto',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
};

class CustomNumberInput extends React.Component {
    constructor(props) {
        super(props);
    }
    saveEnvSettings(payload){
        console.log(payload);
        store.dispatch(actions.setSettingsData(payload));
        setAuthorizationToken(cookie.load('token'));
        axios.put('http://10.30.0.59:3000/settings', payload).then((response) => {
            console.log(response.data);
        }).catch((error) => {
            console.log(error);
        });
    }
    triggerPropChange(direction, key) {
        const settings = this.props.settings;
        if (direction) {
            settings[this.props.propKey] ++;
        } else {
            settings[this.props.propKey] --;
        }
        const payload = {val: settings[this.props.propKey], key};
        this.saveEnvSettings(payload);
    }
    render() {
        return <div style={wrapstyle1}>
            <Clickable onClick={() => this.triggerPropChange(1, this.props.propKey)}>
                <FaUp color="orange" size={60}/>
            </Clickable>
            <FormControl
                type="text"
                value={this.props.settings[this.props.propKey]}
                placeholder={'Set ' + this.props.propKey}
                style={inputStyles}
            />
            <Clickable onClick={() => this.triggerPropChange(0, this.props.propKey)}>
                <FaDown color="aqua" size={60}/>
            </Clickable>
        </div>
    }
}

export default CustomNumberInput;