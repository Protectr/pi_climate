import React from 'react';
import {connect} from "react-redux";
import {initialFetch} from "../actions/actions";
import styled from 'styled-components';
import 'react-circular-progressbar/dist/styles.css';
import io from 'socket.io-client';
import CircularProgressbar from 'react-circular-progressbar';
import FaLightbulb from 'react-icons/lib/fa/lightbulb-o';
import * as actions from "../actions/actions";
import {store} from "../reducers/store";
import CustomNumberInput from "./custom-inputs/customNumberInput";


const SettingsWrap = styled.div`
  font-size: 1.5em;
  text-align: left;
  background-color: #2B3A42;
  height: calc(100vh - 64px);
  width: 100%;
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: stretch;
  flex-flow: column wrap; 
`;

const Clickable = styled.span`
    font-size: 1.5em;
    cursor: pointer;
`;

const Colunm = styled.div`
    height: calc(50vh - 64px/2);
    box-sizing: border-box;
    box-shadow: inset 0px 0px 1px 1px rgba(0,220,255,1);
    position: relative;
`;

const ColInner = styled.div`
       width: 100%;
       max-height: 80%;
       top: 0;
       left: 0;
       text-align: center;
       margin-bottom: 30px;
`;


const wrapstyle2 = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%'

};

class Settings extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount(){
        let socket = io('http://10.30.0.59:3000');
        socket.on('test', (data) => {
            console.log(data);
            store.dispatch(actions.setSensorData(data));
        });
        store.dispatch(actions.initialFetch());
    }

    render() {
        return (
            <SettingsWrap>
                        <Colunm xs={12}>
                            <ColInner>
                                <div style={{ maxWidth: '45%', width: '300px', margin: '10px auto' }}>
                                    <CircularProgressbar
                                        width={34}
                                        percentage={this.props.sensors_data.temperature}
                                        text={`temp: ${this.props.sensors_data.temperature}°`}
                                        styles={{
                                            path: { stroke: `rgb(${this.props.sensors_data.temperature * 3}, 0, 255)` },
                                            text: { fill: '#f9cd12', fontSize: '12px' },
                                            background: {
                                                fill: '#3e9',
                                            },
                                            trail: { stroke: `rgba(100, ${this.props.sensors_data.temperature * 3}, 100, 100)` }
                                        }}
                                    />
                                </div>
                                <CustomNumberInput settings={this.props.settings} propKey={'temperature'}></CustomNumberInput>
                            </ColInner>
                        </Colunm>
                        <Colunm xs={12}>
                            <ColInner>
                                <div style={{ maxWidth: '45%', width: '300px', margin: '10px auto' }}>
                                    <CircularProgressbar
                                        width={34}
                                        percentage={this.props.sensors_data.humidity}
                                        text={`humidity: ${this.props.sensors_data.humidity}%`}
                                        styles={{
                                            path: { stroke: `rgb(${this.props.sensors_data.humidity * 3}, 0, 255)` },
                                            text: { fill: '#f9cd12', fontSize: '12px' },
                                            background: {
                                                fill: '#3e9',
                                            },
                                            trail: { stroke: `rgba(100, ${this.props.sensors_data.humidity * 3}, 100, 100)` }
                                        }}
                                    />
                                </div>
                                <CustomNumberInput settings={this.props.settings} propKey={'humidity'}></CustomNumberInput>
                            </ColInner>
                        </Colunm>
                        <Colunm xs={12}>
                                <div style={wrapstyle2}>
                                    <Clickable>
                                        <FaLightbulb size={100} color="aqua" />
                                    </Clickable>
                                </div>
                        </Colunm>
                        <Colunm xs={12}>
                                <div style={wrapstyle2}>
                                    <Clickable>
                                        <FaLightbulb size={100} color="aqua" />
                                    </Clickable>
                                </div>
                        </Colunm>
            </SettingsWrap>
        )
    }
}

const mapStateToProps = (state) => ({
    settings: state.settings,
    sensors_data: state.sensors_data,
    is_logged: state.is_logged
});

const mapDispatchToProps = {
    initialFetch
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);

