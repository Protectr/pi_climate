import React from 'react';
import { Row, Col, Grid, Button } from 'react-bootstrap';
import styled from 'styled-components';
import { store } from '../reducers/store';
import * as actions from "../actions/actions";
import axios from "axios/index";
import { createHashHistory } from 'history';
import cookie from 'react-cookies';


const LoginWrap = styled.div`
  font-size: 1.5em;
  text-align: left;
  background-color: #2B3A42;
  height: calc(100vh - 64px);
  width: 100%;
  position: relative;
`;

const LoginForm = styled.form`
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
`;

const history = createHashHistory();

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            pw: ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount() {
        console.log('<<<<<<<<<<<<', cookie.load('token'));
        if (cookie.load('token')) {
            this.props.history.push('/settings');
        } else {
            this.props.history.push('/');
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        store.dispatch(actions.loginStarted());
        axios.post('http://10.30.0.59:3000/users/login', this.state).then((response) => {
            store.dispatch(actions.initialFetch);
            store.dispatch(actions.loginSuccess(response.data));
            cookie.save('token', response.data.token, { path: '/', maxAge: '36000' });
        }).catch((error) => {
            store.dispatch(actions.loginError(actions.loginError()));
        });
    }

    handleChange (event) {
        this.state[event.target.name] = event.target.value;
    }

    render() {
        return <LoginWrap>
                    <Grid>
                        <Row>
                            <LoginForm onSubmit={this.handleSubmit} Offset={3}>
                                <Col xs={6} xsOffset={3}>
                                    <input name='name' onChange={event => this.handleChange(event)} placeholder='Login'  type="text"/>
                                </Col>
                                <Col xs={6} xsOffset={3}>
                                    <input name='pw' onChange={event => this.handleChange(event)} placeholder='Password'  type="password"/>
                                </Col>
                                <Col xs={6} xsOffset={3}>
                                    <Button type="submit">Login</Button>
                                </Col>
                            </LoginForm>
                        </Row>
                    </Grid>
                </LoginWrap>

    }
}

export default Login;

