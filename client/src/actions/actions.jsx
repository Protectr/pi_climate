import * as actionTypes from "./types";
import axios from "axios/index";
import setAuthorizationToken from "../utils/setAuthorizationToken";
import cookie from 'react-cookies';

export function loginStarted() {
    return {
        type: actionTypes.LOGIN_START,
    }
}

export function loginSuccess(credentials) {
    return {
        type: actionTypes.LOGIN_SUCCESS,
        payload: credentials
    }
}

export function loginError(error) {
    return {
        type: actionTypes.LOGIN_ERROR,
        payload: error
    }
}


export function fetchStarted() {
    return {
        type: actionTypes.FETCH_STARTED,
    }
}

export function fetchSuccess(settings) {
    return {
        type: actionTypes.FETCH_SUCCESS,
        payload: settings
    }
}

export function fetchError(error) {
    return {
        type: actionTypes.FETCH_ERROR,
        payload: error
    }
}

export function setSensorData(sensordate) {
    return {
        type: actionTypes.SET_SENSOR_DATA,
        payload: sensordate
    }
}

export function setSettingsData(payload) {
    return {
        type: actionTypes.SET_SETTINGS_DATA,
        payload
    }
}

export function initialFetch() {
    return dispatch => {
        setAuthorizationToken(cookie.load('token'));
        dispatch(fetchStarted());
        axios.get('http://10.30.0.59:3000/settings').then((response) => {
            dispatch(fetchSuccess(response.data));
        }).catch((error) => {
            dispatch(fetchError(error));
        });
    }
}
