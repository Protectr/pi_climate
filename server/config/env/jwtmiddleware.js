const jwt = require('jsonwebtoken');
const config = require('../env/index');
const secret = config.jwt.jwtSecret;

const jwtMiddleware = (req, res, next) => {
    const authString = req.headers['authorization'];
    if (typeof(authString) === 'string' && authString.indexOf(' ') > -1) {
        jwt.verify(authString.split(' ')[1], secret, (err, decoded) => {
            if (err) {
                res.sendStatus(403);
                console.log("not decoded");
            } else {
                console.log("decoded");
                req.decoded = decoded;
                next();
            }
        });
    } else {
        res.sendStatus(403);
    }
};

module.exports = jwtMiddleware;