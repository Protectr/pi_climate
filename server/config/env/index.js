module.exports = {
    sqlite: {
        host: 'localhost',
        port: 3306,
        database: 'jwt_auth',
        username: 'root',
        password: 'root',
    },
    jwt: {
        jwtSecret: 'Rustynails_777',
        jwtDuration: '2 hours',
    }
};