const express = require('express');
const streamController = require('../../api/controllers/stream_controller');
const router = express.Router();

router.route('/').get(streamController.get);

router.route('/video').get(streamController.vid);

module.exports = router;