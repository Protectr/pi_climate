const express = require('express');
const settingsCtrl = require('../../api/controllers/settings_controller');
const jwtMiddleware = require('../env/jwtmiddleware');


const router = express.Router();


router.route('/')
    .get(jwtMiddleware, settingsCtrl.get)
    .put(settingsCtrl.update);

module.exports = router;
