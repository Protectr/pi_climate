const express = require('express');
const userCtrl = require('../../api/controllers/user_controller');
const jwtMiddleware = require('../env/jwtmiddleware');

const router = express.Router();

router.route('/register').post(userCtrl.create);
router.route('/login').post(userCtrl.login);

router.route('/:userId')
    .put(jwtMiddleware, userCtrl.update);

router.route('/').delete(jwtMiddleware, userCtrl.remove);

module.exports = router;