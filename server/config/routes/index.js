const express = require('express');
const userRoutes = require('./user');
const streamRoutes = require('./stream');
const settingsRoutes = require('./settings');

const router = express.Router();

router.get('/api-status', (req, res) => res.json({ status: "ok" }));

router.use('/users', userRoutes);
router.use('/stream', streamRoutes);
router.use('/settings', settingsRoutes);

module.exports = router; 