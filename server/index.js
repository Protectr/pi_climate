const app = require('./config/express');
const cors = require('cors');
const bodyParser = require('body-parser');
const socket = require("socket.io");

app.use(cors());

app.use(bodyParser.json());
const port = parseInt(process.env.PORT, 10) || 3000;

const server = app.listen(port, () => {
    console.log(`The server is running at localhost: ${port}`);
});

// socket setup
let io = socket(server);
io.on('connection', (socket) => {
    function gen(){
        socket.emit('test', {
            sensors: {
                temperature: randm(0, 40),
                humidity: randm(0, 100),
            }
        });
    }
    const stertGen = setInterval(function(){gen()}, 2000);
});

// generator
function randm(minimum, maximum){
    return Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
}





module.exports = app;