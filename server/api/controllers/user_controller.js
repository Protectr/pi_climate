const db = require('../models/db_model');
const dbExtra = require('../../config/env/index.js');
const jwt = require('jsonwebtoken');
const secret = dbExtra.jwt.jwtSecret;
var bcrypt = require('bcryptjs');

function login(req, res) {
   const user = db.get('users').find({ name: req.body.name }).value();
   console.log(user, req.body.pw);
   if (user.pw && req.body.pw ) {
           if (bcrypt.compareSync(req.body.pw, user.pw)) {
               const userWithoutPwd = {...user};
               delete userWithoutPwd.pw;
               const token = jwt.sign(userWithoutPwd, secret, {expiresIn: '10h'});
               // to client
               res.status(200).send({
                   user: userWithoutPwd,
                   token: token
               });
           } else {
               res.status(403).send({errorMessage: 'Premission denied - passwords not match!'});
           }
   } else {
       res.status(403).send({errorMessage: 'Premission denied!'});
   }
}

function update(req, res) {
    genUser({name: req.body.name, pw: req.body.pw}, 0);
    return res.status(200).json(res);
}

function create(req, res) {
    if (db.get('users').value().length > 0) {
        return res.status(403).send({message: 'AdminUser already exists! ' + db.get('users').value().length });
    } else {
        genUser({name: req.body.name, pw: req.body.pw}, 1);
        return res.status(200).send({message: 'AdminUser was created! '});
    }
}

function genUser(user, isNewUser) {
    let hash = bcrypt.hashSync(user.pw);
    user.pw = hash;
    console.log('user', user, isNewUser);
    // Store hash in your password DB.
    if (isNewUser) {
        // create user
        db.set('users', [user]).write();
        console.log('User Created --->>', db.get('users').value());
    } else {
        db.get('users').write(user);
        console.log('User Updated --->>', db.get('users').value());
    }
}

function remove(req, res) {
    db.get('users').remove().write();
    console.log('User removed! --->>', db.get('users').value());
    return res.status(200).send({message: 'User removed!'});
}

module.exports = {
    create, remove, update, login
};

