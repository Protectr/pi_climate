const db = require('../models/db_model');

function update(req, res) {
    console.log('', req.body );
    db.set('settings.' + req.body.key, req.body.val).write();
    res.send();
}

function get(req, res) {
    const sts = db.get('settings').value();
    res.status(200).json(sts);
}

module.exports = {
    get, update
};