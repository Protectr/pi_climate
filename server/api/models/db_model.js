const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const adapter = new FileSync('db.json');
const db = low(adapter);

db.defaults({
    users: [{
        name: 'admin1',
        pw: '123123'
    }],
    settings: {
        temperature: 34,
        humidity: 100,
        lights: true,
        innerVent: false,
        exhaustVent: true
    }
}).write();

module.exports = db;


